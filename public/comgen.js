function comgen(options) {
  const Vowels = "aeiouy"

  function sampleString(string, disallow) {
    let result = ""
    while (result == "" || result == disallow) {
      result = string[Math.floor(Math.random() * string.length)]
    }
    return result
  }

  function sampleArray(array) {
    return array[Math.floor(Math.random() * array.length)]
  }

  function syllable(len) {
    const FirstLetters = "bcdfghjklmnprstwz"
    if (len == 2) {
      return sampleString(FirstLetters) + sampleString(Vowels)
    } else if (len == 3) {
      const Lookup = {
        b: "ahijlruz", c: "afhijuz", d: "hiruwz", f: "ilru", g: "adhilnruwz",
        h: "ilr", j: "aiu", k: "ahilruw", l: "hiu", m: "ailru", n: "aiu",
        p: "ahlru", r: "hy", s: "ahilrtuwz", t: "irsuw", w: "ailrt", z: "dilr"
      }
      let first = sampleString(FirstLetters)
      let result = first + sampleString(Lookup[first])
      result += sampleString(Vowels, result[2])
      return result
    }
  }

  let firstSyllables = [];
  {
    options.syllable1 = options.syllable1 || [1, 1]
    let min = options.syllable1[0]
    let max = options.syllable1[1]
    let count = Math.round(min + Math.random() * (max - min))
    for (let i = 0; i < count; ++i) {
      firstSyllables.push(syllable(Math.round(2 + Math.random())))
    }
  }
  return firstSyllables.join("") +
         sampleArray(options.syllable2 || ["me", "ke", "de", "je"]) +
         sampleArray(options.suffix || ["s", "x", "ks"])
}
